/*
 * VIDIX driver for SMedia Glamo chips.
 *
 * Copyright (C) 2008 Andrzej Zaborowski  <balrog@zabor.org>
 * Based on mpeg.c from glamo-utils, written by Chia-I Wu <olv@openmoko.org>
 * Based on ffmpeg code by Fabrice Bellard.
 *
 * This file is part of MPlayer.
 *
 * MPlayer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 or (at your option)
 * version 3 of the License.
 *
 * MPlayer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with MPlayer; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <endian.h>

#include "drivers/libglamo/glamo.h"
#include "drivers/libglamo/hw.h"
#include "drivers/libglamo/dma.h"
#include "drivers/libglamo/blit.h"

#include "config.h"
#include "video_out.h"
#include "video_out_internal.h"
#include "mp_msg.h"
#include "sub/sub.h"
#include "libmpdemux/mpeg4_hdr.h"
#include "subopt-helper.h"

#ifdef X11_FULLSCREEN
#include "x11_common.h"
#endif

static const vo_info_t info = {
	.name		= "SMedia Glamo decoder driver",
	.short_name	= "glamo",
	.author		= "Andrzej Zaborowski <balrog@zabor.org>",
	.comment	= "Experimental, requires root"
};

const LIBVO_EXTERN(glamo)

static uint16_t device_id = 0;

static int query_format(uint32_t fourcc)
{
	if (device_id != 0x3362)
		return 0;

	switch (fourcc) {
	/* MPEG1 stream for our MPEG engine.  */
	case IMGFMT_MPEG4:
	/* Decoded frames to blit onto the framebuffer using the 2D engine
	 * and bypassing MPEG block.  */
	case IMGFMT_I420:
	case IMGFMT_YUY2:
	case IMGFMT_RGB16:	/* TODO */
		return VFCAP_CSP_SUPPORTED | VFCAP_CSP_SUPPORTED_BY_HW |
			VFCAP_HWSCALE_UP | VFCAP_HWSCALE_DOWN | VFCAP_FLIP;
	}

	return 0;
}

static struct blit_ctx_s {
	struct glamo_blit_manager *blit;
	int x;
	int y;
	int width;
	int height;
	int out_width;
	int out_height;
	void *image_buf;

	int blit_started;
} bctx;
static void (*decode_fn)(mp_image_t *mpi);

static int draw_image(mp_image_t *mpi)
{
	struct glamo_blit_image *image;

	decode_fn(mpi);

	image = glamo_blit_new_image(bctx.blit, bctx.image_buf,
			bctx.width, bctx.height);
	bctx.blit_started = 1;
	glamo_blit_show_image(bctx.blit, image, bctx.x, bctx.y,
			bctx.out_width, bctx.out_height);
	glamo_blit_destroy_image(bctx.blit, image);

	return 0;
}

static void flip_page(void)
{
}

static void draw_fb(int x0, int y0, int w, int h, unsigned char *src,
		unsigned char *srca, int stride)
{
	/*
	 * TODO: draw this before the final blit so that we get rotation
	 * right.  This will require writing to bctx.image_buf which may
	 * have already started being blitted.
	 */
	uint8_t *dst = (uint8_t *) glamo_fb +
		 2 * (bctx.x + x0) + glamo_pitch * (bctx.y + y0);

	if (bctx.blit_started) {
		glamo_blit_wait(bctx.blit);
		bctx.blit_started = 0;
	}

	vo_draw_alpha_rgb16(w, h, src, srca, stride, dst, glamo_pitch);
}

static void draw_osd(void)
{
	vo_draw_text(bctx.out_width, bctx.out_height, draw_fb);
}

static int draw_slice(uint8_t *srcimg[], int stride[],
		int w, int h, int x0, int y0)
{
	return 0;
}

static int draw_frame(uint8_t *src[])
{
	return VO_ERROR; 
}

static void check_events(void)
{
#if 0
	int e = vo_check_events();

	mp_msg(MSGT_VO, MSGL_INFO, "got event %x\n", e);
#endif
}

static int get_image(mp_image_t *mpi)
{
	mp_msg(MSGT_VO, MSGL_INFO, "get_image called\n");
	/* TODO: for the non-MPEG4 IMGFMTs return addresses directly
	 * in the Glamo's mmaped memory.  For IMGFMT_MPEG4 we don't gain
	 * anything because the vd_hwmp4 will have to copy all the data
	 * anyway.  */

	return VO_NOTIMPL;
}

static int glamo_window(mp_win_t *win)
{
	bctx.x = win->x;
	bctx.y = win->y;
	bctx.out_width = win->w;
	bctx.out_height = win->h;

	return VO_TRUE;
}

static int control(uint32_t request, void *data)
{
	switch (request) {
	case VOCTRL_QUERY_FORMAT:
		return query_format(*(uint32_t *) data);
	case VOCTRL_DRAW_IMAGE:
		return draw_image((mp_image_t *) data);
	case VOCTRL_GET_IMAGE:
		return get_image(data);

	/* "xover" support */
	case VOCTRL_XOVERLAY_SUPPORT:
	case VOCTRL_XOVERLAY_SET_COLORKEY:
		return VO_TRUE;
	case VOCTRL_XOVERLAY_SET_WIN:
		return glamo_window((mp_win_t *) data);
	}

	return VO_NOTIMPL;
}

static strarg_t fb_path = { 0, NULL };
static int mpeg_db_thr[2] = { 0, 0 };
static int mpeg_deblock;
static int angle = 0;
static enum glamo_blit_rotation rotate;
static int busywait = 0;

static opt_t glamo_subopts[] = {
	{ "deblock1", OPT_ARG_INT,  &mpeg_db_thr[0], (opt_test_f) int_pos, 0 },
	{ "deblock2", OPT_ARG_INT,  &mpeg_db_thr[1], (opt_test_f) int_pos, 0 },
	{ "rotate",   OPT_ARG_INT,  &angle,          (opt_test_f) int_pos, 0 },
	{ "fb",       OPT_ARG_STR,  &fb_path,        NULL,                 0 },
	{ "nosleep",  OPT_ARG_BOOL, &busywait,       NULL,                 0 },

	{ 0, 0, 0, 0, 0 }
};

static int preinit(const char *arg)
{
	uint16_t dev_id, rev_id;
	int err;

	err = subopt_parse(arg, glamo_subopts);
	if (err)
		return err;

	err = glamo_os_init(fb_path.str);
	if (err)
		return err;

	mpeg_deblock = mpeg_db_thr[0] || mpeg_db_thr[1];

	switch (angle % 360) {
	case 0:
		rotate = GLAMO_BLIT_ROTATION_0;
		break;
	case 90:
		rotate = GLAMO_BLIT_ROTATION_90;
		break;
	case 180:
		rotate = GLAMO_BLIT_ROTATION_180;
		break;
	case 270:
		rotate = GLAMO_BLIT_ROTATION_270;
		break;
	default:
		mp_msg(MSGT_VO, MSGL_ERR,
				"unsupported angle: %i deg\n", angle);
		exit(-1);
	}

	dev_id = GLAMO_IN_REG(GLAMO_REG_DEVICE_ID);
	rev_id = GLAMO_IN_REG(GLAMO_REG_REVISION_ID);
	glamo_os_finish();

	switch (dev_id) {
	case 0x3600:
	case 0x3650:
	case 0x3700:
		device_id = 0x3362;
		dev_id = 0x3000 | (dev_id >> 4);
		break;
	default:
		return -ENODEV;
	}

	mp_msg(MSGT_VO, MSGL_INFO,
			"Found SMedia Glamo %x, with %x core, rev A%i\n",
			device_id, dev_id, rev_id);
	return 0;
}

void inline st32le(void *ptr, uint32_t value)
{
#if __BYTE_ORDER == __LITTLE_ENDIAN
# if 0
	memcpy(ptr, &value, 4);
# else
	*(uint32_t *) ptr = value;
# endif
#else
	((uint8_t *) ptr)[0] = (value >>  0) & 0xff;
	((uint8_t *) ptr)[1] = (value >>  8) & 0xff;
	((uint8_t *) ptr)[2] = (value >> 16) & 0xff;
	((uint8_t *) ptr)[3] = (value >> 24) & 0xff;
#endif
}

static void yield1(void)
{
	const struct timespec req = { 0, 500000 };

	if (!busywait)
		nanosleep(&req, 0);
}

static void glamo_mpeg_wait(void)
{
	uint16_t status;

	while ((status = GLAMO_IN_REG(GLAMO_REG_MPEG_DEC_STATUS)) & 0x3000)
		yield1();

	if (status & 0x3c0)
		mp_msg(MSGT_VO, MSGL_ERR, "status = [%s%s%s%s%s%s%s ]\n",
				(status & 0x000f) ? " ErrorOldFrame" : "",
				(status & 0x0030) ? " OldFrameIndex" : "",
				(status & 0x03c0) ? " ErrorThisFrame" : "",
				(status & 0x0c00) ? " ThisFrameIndex" : "",
				(status & 0x1000) ? " QueueFull" : "",
				(status & 0x2000) ? " EngineBusy" : "",
				(status & 0xc000) ? " DebugInfo" : "");
}

static void glamo_isp_wait(void)
{
	uint16_t status;

	while ((status = GLAMO_IN_REG(GLAMO_REG_ISP_EN1)) &
			GLAMO_ISP_EN1_FIRE_CAP)
		yield1();

	while ((status = GLAMO_IN_REG(GLAMO_REG_ISP_STATUS)) & 1)
		yield1();

	bctx.blit_started = 0;
}

struct yuv_buffer {
	unsigned int y;
	unsigned int u;
	unsigned int v;
};

struct mpeg_ctx_s {
	int width;
	int height;

	int cycle;
	int reg_special;

	unsigned int buf_in;
	struct yuv_buffer buf_out[2];
	unsigned int buf_dc;
	unsigned int buf_ac;

	struct glamo_dma_manager *dma;
} mctx;

static void decode_null(mp_image_t *mpi)
{
	uint8_t *vram;

	vram = (uint8_t *) glamo_fb + mctx.buf_out->y;
	memcpy(vram, mpi->planes[0], mctx.width * mctx.height);
	vram = (uint8_t *) glamo_fb + mctx.buf_out->u;
	memcpy(vram, mpi->planes[1], (mctx.width * mctx.height) >> 2);
	vram = (uint8_t *) glamo_fb + mctx.buf_out->v;
	memcpy(vram, mpi->planes[2], (mctx.width * mctx.height) >> 2);
}

static void decode_mpeg(mp_image_t *mpi)
{
	mp_mpeg_header_t *picture = (void *) mpi->planes[1];
	uint32_t glamo_mpeg_header;
	uint8_t *vram;
	int cycle = (mctx.cycle ++) & 1;

	/* Wait until ready */
	glamo_mpeg_wait();

	glamo_hw_engine_reset(GLAMO_ENGINE_MPEG);

	if (mctx.reg_special != picture->vol.time_bits) {
		/* XXX: for Short Header always equals 8? */
		mctx.reg_special = picture->vol.time_bits;
		GLAMO_OUT_REG(GLAMO_REG_MPEG_SPECIAL,
				(mctx.reg_special << 8) |
				(mpeg_deblock << 3));
	}

	glamo_mpeg_header =
		(picture->vop.start_bits << 0) |
		(picture->vop.rounding << 3) |
		((picture->vop.fcode ?
		  picture->vop.fcode - 1 : 0) << 4) |
		(picture->vop.coding_type << 7) |
		(picture->vop.qscale << 8) |
		(picture->vop.intra_dc_thr << 13) |
		(cycle << 20) | ((!cycle) << 22);

	/* Upload the frame.
	 *
	 * TODO: use CPU's DMA for this possibly,
	 * TODO: seems that the memcpy size cannot exceed 64K.
	 * TODO: if an error was detected in glamo_mpeg_wait(), the
	 * next frame must be based on the previous frame instead of the
	 * current erroneous one, as a form of recovery.  We will need
	 * more buffers for this.
	 */
	vram = (uint8_t *) glamo_fb + mctx.buf_in;
	st32le(vram, glamo_mpeg_header);
	memcpy(vram + 4, picture->data_start, picture->data_size);
	vram += 4 + picture->data_size;
	/* MPEG terminating header */
	st32le(vram, 0xb6010000);

	/* Wait until ready so we can start the blit quickly */
	if (bctx.blit_started)
		glamo_isp_wait();

	/* Decode! */
	GLAMO_OUT_REG(GLAMO_REG_MPEG_DEC_IN_ADDRL,
			(mctx.buf_in >> 1) & 0xffff);
	GLAMO_OUT_REG(GLAMO_REG_MPEG_DEC_IN_ADDRH,
			((mctx.buf_in >> 17) & 0x3f) | (cycle << 8));

	bctx.image_buf = (void *) mctx.buf_out[!cycle].y;
}

#define OUT_ADDR(reg, addr)	do { 					\
		GLAMO_OUT_REG((reg),     ((addr) >>  1) & 0xffff);	\
		GLAMO_OUT_REG((reg) + 2, ((addr) >> 17) & 0x3f);	\
	} while (0)

static inline int binlog2(int num)
{
	int log;

	for (log = 0; num > (1 << log); log ++);

	return log;
}

static int config(uint32_t width, uint32_t height, uint32_t d_width,
		uint32_t d_height, uint32_t flags, char *title,
		uint32_t fourcc)
{
	int err;
	int pos;
	int pitch;
	int num_mbs;

	if (!vo_config_count) {
		err = glamo_os_init(fb_path.str);
		if (err)
			return -err;

		GLAMO_OUT_REG(GLAMO_REG_CLOCK_ISP, 0x0000);
		GLAMO_OUT_REG(GLAMO_REG_CLOCK_ISP, 0x0000);
		GLAMO_OUT_REG(GLAMO_REG_CLOCK_MPEG, 0x0000);
		GLAMO_OUT_REG(GLAMO_REG_CLOCK_MPEG, 0x0000);
		usleep(10000);
		glamo_hw_engine_reset(GLAMO_ENGINE_MPEG);

		glamo_hw_engine_enable(GLAMO_ENGINE_MPEG);
		mctx.dma = glamo_dma_new(GLAMO_DMA_MODE_MMIO);
	}

	if (vo_config_count) {
		glamo_blit_destroy(bctx.blit);
	}

	mctx.width = width;
	mctx.height = height;

	bctx.width = width;
	bctx.height = height;
	bctx.out_width = d_width;
	bctx.out_height = d_height;
	if (!(flags & VOFLAG_XOVERLAY_SUB_VO))
		bctx.x = bctx.y = 0;

	/* TODO: only use GLAMO_VRAM_MPEG for MPEGs */
	/* Three planes for the output from the decoder and input to BES.  */
	pos = GLAMO_VRAM_MPEG;
	mctx.buf_out->y = pos;
	pos += width * height;
	mctx.buf_out->u = pos;
	pos += (width * height) >> 2;
	mctx.buf_out->v = pos;
	pos += (width * height) >> 2;

	bctx.image_buf = (void *) mctx.buf_out->y;
	bctx.blit_started = 0;

	switch (fourcc) {
	case IMGFMT_MPEG4:
		/* TODO: more checks */
		if (((width > 640 || height > 480) &&
					(width > 480 && height > 640)) ||
				((width | height) & 15)) {
			mp_msg(MSGT_VO, MSGL_ERR, "bad image size\n");
			exit(-1);
		}

		decode_fn = decode_mpeg;

		mctx.reg_special = -1;

		/* Three planes for decoder output swap buffer.  */
		mctx.buf_out[1].y = pos;
		pos += width * height;
		mctx.buf_out[1].u = pos;
		pos += (width * height) >> 2;
		mctx.buf_out[1].v = pos;
		pos += (width * height) >> 2;

		/* DC prediction buffer */
		mctx.buf_dc = pos;
		pos += (width >> 4) * 6;

		/* AC prediction buffer */
		mctx.buf_ac = pos;
		pos += (width >> 4) * 4 * 12;

		/* TODO: allocate deblocking framebuffers here */

		/* MPEG data buffer */
		mctx.buf_in = pos;

		bctx.blit = glamo_blit_new(mctx.dma, GLAMO_BLIT_FORMAT_I420);

		OUT_ADDR(GLAMO_REG_MPEG_DC_ADDRL, mctx.buf_dc);
		OUT_ADDR(GLAMO_REG_MPEG_AC_ADDRL, mctx.buf_ac);

		OUT_ADDR(GLAMO_REG_MPEG_DEC_OUT0_Y_ADDRL, mctx.buf_out[0].y);
		OUT_ADDR(GLAMO_REG_MPEG_DEC_OUT0_U_ADDRL, mctx.buf_out[0].u);
		OUT_ADDR(GLAMO_REG_MPEG_DEC_OUT0_V_ADDRL, mctx.buf_out[0].v);
		OUT_ADDR(GLAMO_REG_MPEG_DEC_OUT1_Y_ADDRL, mctx.buf_out[1].y);
		OUT_ADDR(GLAMO_REG_MPEG_DEC_OUT1_U_ADDRL, mctx.buf_out[1].u);
		OUT_ADDR(GLAMO_REG_MPEG_DEC_OUT1_V_ADDRL, mctx.buf_out[1].v);

		pitch = width;
		GLAMO_OUT_REG(GLAMO_REG_MPEG_DEC_WIDTH,
				(width >> 4) | ((pitch >> 1) << 7));

		/* Yes, UV pitch is left shifted by 8 bits.  */
		pitch = width >> 1;
		GLAMO_OUT_REG(GLAMO_REG_MPEG_DEC_HEIGHT,
				(height >> 4) | ((pitch >> 1) << 8));

		num_mbs = binlog2((width >> 4) * (height >> 4)) + 1;
		GLAMO_OUT_REG(GLAMO_REG_MPEG_SAFE_1, num_mbs << 11);
		GLAMO_OUT_REG(GLAMO_REG_MPEG_SAFE_3, 0xf00 | (1 << 15));
		GLAMO_OUT_REG(GLAMO_REG_MPEG_DEBLK_THRESHOLD,
				mpeg_db_thr[0] | (mpeg_db_thr[1] << 8));
		break;

	case IMGFMT_I420:
		decode_fn = decode_null;

		bctx.blit = glamo_blit_new(mctx.dma, GLAMO_BLIT_FORMAT_I420);
		break;

	case IMGFMT_YUY2:
		decode_fn = decode_null;

		bctx.blit = glamo_blit_new(mctx.dma, GLAMO_BLIT_FORMAT_YUY2);
		break;

	default:
		return EINVAL;
	}

	glamo_blit_rotate(bctx.blit, rotate);

	return 0;
}

static void uninit(void) {
	if (!vo_config_count)
		return;

	glamo_blit_destroy(bctx.blit);
	glamo_dma_destroy(mctx.dma);

	glamo_hw_engine_disable(GLAMO_ENGINE_MPEG);
	glamo_os_finish();
}
