/*
 * Copyright (C) 2008 Andrzej Zaborowski  <balrog@zabor.org>
 *
 * This file is part of MPlayer.
 *
 * MPlayer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 or (at your option)
 * version 3 of the License.
 *
 * MPlayer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with MPlayer; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#ifndef MPLAYER_MPEG4_HDR_H
#define MPLAYER_MPEG4_HDR_H

/*
 * We redefine mp_mpeg_header_t and VOL/VOP decoding functions, which
 * are already in "libmpdemux/mpeg_hdr.h" because they discard most of the
 * important info while parsing.
 */
typedef struct mp_mpeg_header_s {
	struct {
		int vo_type;
		int aspect_ratio;
		int shape;
		int ver_id;
		int time_den;
		int time_num;
		int time_bits;
		int width;
		int height;
		int quant_precision;
		int resync_marker;
		int data_partitioning;
		int rvlc;
	} vol;
	struct {
		int coding_type;
		int rounding;
		int time_base;
		int time_inc;
		int vop_coded;
		int intra_dc_thr;
		int qscale;
		int fcode;
		int start_bits;
	} vop;
	void *data_start;
	void *data_end;
	int data_size;
	int valid_vop, valid_vol;
} mp_mpeg_header_t;

#if 0
int mp4_header_process_vol(mp_mpeg_header_t *picture, uint8_t **buffer);
int mp4_header_process_vop(mp_mpeg_header_t *picture, uint8_t **buffer);
#endif

#endif /* MPLAYER_MPEG4_HDR_H */
