/*
 * Library's linux interface.
 * Currently FIC GTA02 specific.
 *
 * Copyright (C) 2007 OpenMoko, Inc.
 * Author: Chia-I Wu <olv@openmoko.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 or
 * (at your option) version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <errno.h>

#include "hw.h"

#define FB_SIZE 0x800000

static int fd_mem;
static int fd_fb;
static struct fb_fix_screeninfo fix;

volatile unsigned char *glamo_fb;
volatile unsigned char *glamo_mmio;
int glamo_pitch;

#ifndef FB_ACCEL_GLAMO
# define FB_ACCEL_GLAMO		50	/* SMedia Glamo                 */
#endif

int glamo_os_init(const char *fb_path)
{
	if (!fb_path || !*fb_path)
		fb_path = "/dev/fb0";

	fd_fb = open(fb_path, O_RDWR | O_SYNC);
	if (fd_fb < 0)
	{
		printf("error opening %s: %s\n", fb_path, strerror(errno));
		return -errno;
	}

	if (ioctl(fd_fb, FBIOGET_FSCREENINFO, &fix) < 0)
	{
		printf("error in FBIOGET_FSCREENINFO: %s\n", strerror(errno));

		close(fd_fb);
		return -errno;
	}

	if (fix.accel != FB_ACCEL_GLAMO) {
		printf("No Glamo chip found or the kerne is old.\n");
		/* TODO: Bail out, once the FB_ACCEL_GLAMO #define goes
		 * into the kernel.  */
	}

	/* hard-coded */
	fix.smem_len = FB_SIZE;
	glamo_pitch = fix.line_length;

	fd_mem = open("/dev/mem", O_RDWR | O_SYNC);
	if (fd_mem < 0)
	{
		printf("error opening mem: %s\n", strerror(errno));

		close(fd_fb);
		return -errno;
	}

	/* TODO: use map_phys_mem and unmap_phys_mem */
	glamo_fb = mmap(NULL, fix.smem_len, PROT_READ | PROT_WRITE,
			MAP_SHARED, fd_mem, fix.smem_start);
	if (glamo_fb == MAP_FAILED)
	{
		printf("error mmap'ping fb: %s\n", strerror(errno));

		munmap((void *) glamo_fb, fix.smem_len);
		close(fd_mem);
		close(fd_fb);
		return -errno;
	}

	/* TODO: use map_phys_mem and unmap_phys_mem */
	glamo_mmio = mmap(NULL, GLAMO_MMIO_SIZE, PROT_READ | PROT_WRITE,
			MAP_SHARED, fd_mem, GLAMO_MMIO_BASE);
	if (glamo_mmio == MAP_FAILED)
	{
		printf("error mmap'ping mmio: %s\n", strerror(errno));

		munmap((void *) glamo_fb, fix.smem_len);
		close(fd_mem);
		close(fd_fb);
		return -errno;
	}

#if 0
	printf("fb   0x%08lx (0x%x)\n"
	       "mmio 0x%08x (0x%x)\n",
	       fix.smem_start, fix.smem_len,
	       GLAMO_MMIO_BASE, GLAMO_MMIO_SIZE);
#endif
	return 0;
}

void glamo_os_finish(void)
{
	munmap((void *) glamo_mmio, GLAMO_MMIO_SIZE);
	munmap((void *) glamo_fb, fix.smem_len);

	close(fd_fb);
	close(fd_mem);
}
