/*
 * Glamo chip access library (for graphics).
 *
 * Copyright (C) 2007 OpenMoko, Inc.
 * Author: Chia-I Wu <olv@openmoko.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 or
 * (at your option) version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */
#ifndef _GLAMO_H_
#define _GLAMO_H_

#define GLAMO_OUT_REG(reg, val) (*((volatile unsigned short *) ((glamo_mmio) + (reg))) = (val))
#define GLAMO_IN_REG(reg) (*((volatile unsigned short *) ((glamo_mmio) + (reg))))

extern volatile unsigned char *glamo_fb;
extern volatile unsigned char *glamo_mmio;
extern int glamo_pitch;

extern int mydiv(int n, int d);
extern int myrem(int n, int d);

inline static void glamo_set_bit_mask(int reg, int mask, int val)
{
	int old;

	old = GLAMO_IN_REG(reg);
	old &= ~mask;
	old |= val & mask;
	GLAMO_OUT_REG(reg, old);
}

int glamo_os_init(const char *fb_path);
void glamo_os_finish(void);

#endif /* _GLAMO_H_ */
